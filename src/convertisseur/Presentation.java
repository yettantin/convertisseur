package convertisseur;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Scanner;

public class Presentation {

	public static void main(String[] args) {
		
		// Déclarer les variables
		double tempAConvertir = 0;
		char typeConversion = ' ';
		char faireConversion = ' ';
		
		// Instancier Scanner
		Scanner sc = new Scanner(System.in);
		
		// Instancier Metier
		Calculateur tempConvertie = new Calculateur(tempAConvertir);
				
		// Afficher un titre
		AfficherTitre();
		
		do {
			// Choix du type de conversion
			typeConversion = choixTypeConversion(sc);
						
			// Récupérer la valeur à convertir
			tempAConvertir = aConvertir(sc);
			
			// Convertir la saisie
			if (typeConversion == '1') {
				double tempConvertieTraite = tempConvertie.convertirCelEnFar(tempAConvertir);
				AfficherResultatFar(tempAConvertir, tempConvertieTraite);
			} else {
				double tempConvertieTraite = tempConvertie.convertirFarEnCel(tempAConvertir);
				AfficherResultatCel(tempAConvertir, tempConvertieTraite);
			} 
			
			// Demander de recommencer
			faireConversion = choixFaireConversion(sc);
									
		} while(faireConversion == 'O');
		
		// Fermer le scanner
		sc.close();
			
		// Afficher au revoir
		AuRevoir();		
	}
	
	public static void AfficherTitre() {
		System.out.println("------------------------------------");
		System.out.println("- CONVERTISSEUR CELCIUS FAHRENHEIT -");
		System.out.println("------------------------------------");
	}
	
	public static void AuRevoir() {
		System.out.println("------------------------------------");
		System.out.println("-            A bientôt !           -");
		System.out.println("------------------------------------");
	}
	
	public static char choixTypeConversion(Scanner sc) {
		char reponseTypeConversion = '1';
		
		System.out.println("Quelle conversion souhaitez vous faire?");
		System.out.println("1 - De celcius en fahrenheint");
		System.out.println("2 - De fahrenheint en celcius");
		
		do {
			// récupérer la réponse
			reponseTypeConversion = sc.nextLine().charAt(0);
			
			// s'assurer de la validité de la réponse
			if(reponseTypeConversion == '1' || reponseTypeConversion == '2') {
				return reponseTypeConversion;		
			}
			else {
				System.out.println("Veuillez répondre 1 ou 2");
			}
		} while(reponseTypeConversion != '1' && reponseTypeConversion != '2');
			
	return reponseTypeConversion;
	}
	
	public static char choixFaireConversion(Scanner sc) {
		
		char faireConversion = 'O';
		
		System.out.println("Souhaitez vous faire une nouvelle convertion ? (O/N)");
			
		do {
			// récupérer la réponse
			faireConversion = sc.nextLine().charAt(0);
						
			// s'assurer de la validité de la réponse
			if(faireConversion == 'O' || faireConversion == 'N') {
				return faireConversion;		
			}
			else {
				System.out.println("Veuillez répondre O ou N");
			}
		} while(faireConversion != 'O' && faireConversion != 'N');			
		
		return faireConversion; 	
	}		
	
	public static double aConvertir(Scanner sc) {
		String valeurAValider = "";
		boolean valeurTeste = false;
		double valeurAConvertir = 0;
		
		// Demander la valeur à convertir
		System.out.println("Entrer la température à convertir");
	
		// Valider la valeur saisie
		do {
			// Récupérer la saisie
			valeurAValider = sc.nextLine();
			
			//tester la validité de la valeur
			valeurTeste = isNumeric(valeurAValider);
			
			if(valeurTeste) {
				valeurAConvertir = Double.parseDouble(valeurAValider);
			}
			else {
				System.out.println("Veuillez rentrer un nombre");
			}
		} while(valeurTeste == false);	
			
		// Retourner la valeur à convertir
		return valeurAConvertir;
	}
	
	public static boolean isNumeric(String strNum) {
	    try {
//	    	// code d'origine
//	    	double d = Double.parseDouble(strNum);
	    	
	    	Double.parseDouble(strNum);

	    } catch (NumberFormatException | NullPointerException nfe) {
	        return false;
	    }
	    return true;
	}
		
	public static String convertirDoubleToString(double dToStr) {	
		
		// 
		NumberFormat nf = NumberFormat.getNumberInstance();
		
		// Déterminer le nb de décimal
		nf.setMaximumFractionDigits(1);
		
		
		// Déterminer le mode d'arrondi
		nf.setRoundingMode(RoundingMode.HALF_UP);
		
		// Convertir le double en String
		String resultat = nf.format(dToStr);
		
		return resultat;
	}
	
	public static void AfficherResultatFar(double tempAConvertir,double tempConvertie) {
		System.out.println(tempAConvertir + "°C équivaut à : " + tempConvertie + "°F.");
	}
	
	public static void AfficherResultatCel(double tempAConvertir,double tempConvertie) {
		System.out.println(tempAConvertir + "°F équivaut à : " + tempConvertie + "°C.");
	}
}
	
	